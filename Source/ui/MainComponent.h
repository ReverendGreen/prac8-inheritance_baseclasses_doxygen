/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "Counter.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Button::Listener,
                        public Counter::Listener,
                        public Slider::Listener,
                        public ComboBox::Listener

{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& audio_);

    /** Destructor */
    ~MainComponent();
    
    
  


    void resized() override;
    
    
    //Button Functions========================================================
    
    void buttonClicked (Button*) override;
    
    void sliderValueChanged (Slider* slider) override;
    
  
//    void buttonStateChanged (Button* button)  override;
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
    void counterChanged (const unsigned int counterValue) override;
    
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
private:
    Audio& audio;
    Slider ampSlider, bpmSlider, metroAmpSlider, dutyCycleSlider;
    Label ampLabel, bpmLabel, dutyCycleLabel;
    ToggleButton metroButton;
    Counter counterThread;
    ComboBox waveBox;
    
    

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED

/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
                                               

{
//    ampSlider.setRange(0.0f, 1.0f);
//    addAndMakeVisible(ampSlider);
//    ampSlider.addListener(this);
//    
    ampLabel.setText("Amplitude", dontSendNotification);
    ampLabel.attachToComponent(&metroAmpSlider, false);
    bpmLabel.setText("BPM", dontSendNotification);
    bpmLabel.attachToComponent(&bpmSlider, false);
    dutyCycleLabel.setText("Duty Cycle", dontSendNotification);
    dutyCycleLabel.attachToComponent(&dutyCycleSlider, false);
    
    
    metroButton.setButtonText("Metronome On/Off");
    metroButton.addListener(this);
    addAndMakeVisible(metroButton);
    metroButton.setToggleState(false, dontSendNotification);
    
    bpmSlider.setRange(60.f, 350.f);
    addAndMakeVisible(bpmSlider);
    bpmSlider.addListener(this);
    
    metroAmpSlider.setRange(0.0, 1.0);
    metroAmpSlider.setValue(0.5);
    addAndMakeVisible(metroAmpSlider);
    metroAmpSlider.addListener(this);
    
    dutyCycleSlider.setRange(0.0, 1.0);
    addAndMakeVisible(dutyCycleSlider);
    dutyCycleSlider.addListener(this);
    dutyCycleSlider.setValue(0.5);
    
    waveBox.addItem("Sine Wave", 1);
    waveBox.addItem("Square Wave", 2);
    addAndMakeVisible(waveBox);
    waveBox.addListener(this);
    waveBox.setSelectedId(1);
    
    
    
    
    
    counterThread.setListener (this);
    
    setSize (500, 400);

}

MainComponent::~MainComponent()

{
    counterThread.stopThread(500);

}


void MainComponent::resized()

{
    
    metroButton.setBounds(30, 30, getWidth() / 5, 50);
    metroAmpSlider.setBounds(30, 100, getWidth()/2, 30);
    bpmSlider.setBounds(30 , 200, getWidth()/2, 30);
    dutyCycleSlider.setBounds(30, 300, getWidth()/2, 30);
    waveBox.setBounds(getWidth() /2 ,30,  getWidth()/2 - 40, 30);
    
}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &metroButton)
    {
        
        bool state = metroButton.getToggleState();
        DBG (state);
        std::cout << state << std::endl;
        
        
        if (state)
        {
            counterThread.startThread();
        }
        else
        {
            counterThread.stopThread(500);
        }
    }

}

void MainComponent::sliderValueChanged (Slider* slider)

{
    if (slider == &bpmSlider)
        
    {
        counterThread.setInterval(bpmSlider.getValue());
    }
    
    else if (slider == &dutyCycleSlider)
        
    {
        audio.setWaveDutyCycle(dutyCycleSlider.getValue());
    }
    
    else if (slider == &metroAmpSlider)
        
    {
        audio.setGlobalAmp(metroAmpSlider.getValue());
    }
}






//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}


void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

void MainComponent::counterChanged (const unsigned int counterValue)
{
    DBG("Count:" << (int)counterValue);
    audio.metro(metroAmpSlider.getValue());
}

void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)

{
    if (comboBoxThatHasChanged == &waveBox)
    
    {
        
        if (waveBox.getSelectedId() == 1)
        
        {
            audio.setWaveType(Audio::SIN_OSC);
            dutyCycleLabel.setVisible(false);
            dutyCycleSlider.setVisible(false);

        }
        
        else if (waveBox.getSelectedId() == 2)
            
        {
            audio.setWaveType(Audio::SQUARE_WAVE);
            dutyCycleLabel.setVisible(true);
            dutyCycleSlider.setVisible(true);
            audio.setWaveDutyCycle(0.5);
            
        }
    }
}


//
//  Counter.h
//  JuceBasicAudio
//
//  Created by Neil McGuiness on 27/10/2016.
//
//

#ifndef Counter_h
#define Counter_h

#include "../JuceLibraryCode/JuceHeader.h"

class Counter : public Thread

{
    
public:
    
    // Listener base class for Counter::Listeners. DECLARE CLASS BEFORE ANY MENTIONS OF THE "Listener" variabe in other Counter functions (such as setListener()) or before delacring a Listener in the private: section.
    class Listener
    
    {
    public:
        
        /** Destructor. */ virtual ~Listener() {}
        
        // Pure virtual function that must be implemented in whatever class inherits this Listener.
        /** Called when the next timer has reached the next interval. */
        virtual void counterChanged (const unsigned int counterValue) = 0;
        
    };
    
    Counter();
    ~Counter();
    
    // My 'Thread' pure virtual function that needs to be implemented

    
    void run() override;
    
    // This is analogous to the addListener() function seen in other components.
    void setListener (Listener* newlistener);
    
    void setInterval(float bpm);
    
    
   
    
private:
    // Declaring a pointer to a Listener here.
    Listener* listener;
    int counterValue;
    float interval;
    
    /** Class for counter listeners to inherit */

    
};

#endif /* Counter_h */

//
//  Oscillator.h
//  JuceBasicAudio
//
//  Created by Neil McGuiness on 03/11/2016.
//
//

#ifndef H_OSCILLATOR
#define H_OSCILLATOR

/**  Simple Abstract base class of an Osciallator for subsequent oscillator types to inherit from and develop.
 
 @see SinOscillator
 @see SquareOcillator
 
 */

class Oscillator
{
public:
    //==============================================================================
    /**
     Oscillator constructor
     */
    Oscillator();
    
    /**
     Oscillator destructor
     */
    virtual ~Oscillator();
    
    /**
     sets the frequency of the oscillator
     
     @param freq          chosen frequency input by the user.
     */
    void setFrequency (float freq);
    
    /**
     sets frequency using a midi note number
     
     @param noteNum       MIDI note number input.
     */
    void setNote (int noteNum);
    
    /**
     sets the amplitude of the oscillator
     
     @param amp           floating point number 0.0 - 1.0 to determine amplitue of osc
     */
    void setAmplitude (float amp);
    
    /**
     resets the oscillator
     */
    void reset();
    
    /**
     sets the sample rate
     
     @param sr            sample rate of the host used to determine phase increment in nextSample()
     
     @see nextSample
     */
    void setSampleRate (float sr);
    
    /**
     Returns the next sample
     
     @return float        returns the output sample of the  renderWaveShape() function scaled by amplitude.
     @see renderWaveShape
     */
    float nextSample();
    
    /**
     
     Pure virtual function that provides the execution of the waveshape
     
     @param currentPhase  current phase passed in from nextSample()
     @see nextSample
     @return              returns the rendered sample of the wave at poisition of the currentPhase param.
     */
    
    virtual float renderWaveShape (const float currentPhase) = 0;
    
    /**  
     
     A virtual function to set the duty cycle of Pulsewave/SquareWave classes. 
     
     @param newDutyCycle  variable to determine the dutyCycle of the wave (sqaure/pulewave only!)
     
     */
    
    virtual void setDutyCycle(const float newDutyCycle);
    
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phase;
    float phaseInc;
};



#endif /* H_OSCILLATOR */

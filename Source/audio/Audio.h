/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "SinOscillator.h"
#include "SquareOscillator.h"
#include "Oscillator.h"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    enum WAVE_TYPE
    {
        SIN_OSC = 0,
        SQUARE_WAVE = 1
        
    };
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
    
    void metro(float amp);
    
    void setGlobalAmp(float newAmplitude);
    
    void setWaveDutyCycle(float dutyCycle);
    
    void setWaveType(WAVE_TYPE);
    
   
    
    
private:
    AudioDeviceManager audioDeviceManager;
    
    Atomic <Oscillator*> pOsc;
    SinOscillator sinOsc;
    SquareOscillator squareOsc;
    Atomic<float> frequency;
    Atomic<float> amplitude;
    float phasePosition;
    float sampleRate;
    int lastNote;
};



#endif  // AUDIO_H_INCLUDED

/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio() : pOsc (&sinOsc)
{
    pOsc = &sinOsc;
    
    StringArray MidiDevices = MidiInput::getDevices();
    
    for (int x = 0 ; x < MidiDevices.size(); x ++)
    
    {
        audioDeviceManager.setMidiInputEnabled(MidiDevices[x], true);
        DBG(MidiDevices[x] + "\n");
    }
    

    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    if (message.isNoteOnOrOff())
        
    {
        DBG(String(message.getNoteNumber()) + "\n" + String (message.getFloatVelocity()) + "\n");
        pOsc.get()->setNote((message.getNoteNumber()));
        
        if (message.getFloatVelocity() == 0)
        
        {
            if ( message.getNoteNumber() == lastNote)
                
            {
                pOsc.get()->setAmplitude(message.getFloatVelocity());
                lastNote = message.getNoteNumber();
                
                
                
            }
        
        }
        
        else
            
        {
            pOsc.get()->setAmplitude(message.getFloatVelocity());
        }
        
        
        lastNote = message.getNoteNumber();
        
        
    }
}




void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    

    
    while(numSamples--)
    {

        
        *outL = pOsc.get()->nextSample() * amplitude.get();
        *outR = pOsc.get()->nextSample() * amplitude.get();
        
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    pOsc.get()->setSampleRate(device->getCurrentSampleRate());
    
}

void Audio::audioDeviceStopped()
{

}

void Audio::metro(float amp)
{
    uint32 now = Time::getMillisecondCounter();
    
    pOsc.get()->setAmplitude(amp);
    pOsc.get()->setFrequency(440);
    
    Time::waitForMillisecondCounter(now + 100);
    pOsc.get()->setAmplitude(0);

}

void  Audio::setGlobalAmp(float newAmplitude)

{
    amplitude.set(newAmplitude);
    float currentamp = amplitude.get();
    DBG(String(currentamp));
}

void  Audio::setWaveDutyCycle(float dutyCycle)

{
    pOsc.get()->setDutyCycle(dutyCycle);
}

void Audio::setWaveType(Audio::WAVE_TYPE waveType)

{
    if (waveType == SIN_OSC)
    
    {
        pOsc = &sinOsc;
    }
    
    else if (waveType == SQUARE_WAVE)
        
    {
        pOsc = &squareOsc;
    }
}


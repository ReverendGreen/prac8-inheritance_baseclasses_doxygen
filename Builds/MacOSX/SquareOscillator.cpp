/*
 *  SquareOscillator.cpp
 *  sdaAudioMidi
 *
 *  Created by Neil McGuiness 
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "SquareOscillator.h"
#include <cmath>

SquareOscillator::SquareOscillator() : twoPi(2 * M_PI)

{
    
}


float SquareOscillator::renderWaveShape (const float currentPhase)
{
    float out;
    
    if (currentPhase < dutyCycle.get())
    
    {
        out = 1;
    }
    
    else
        
    {
        out = - 1;
    }
    
    
	return out;
}


void SquareOscillator::setDutyCycle(float newDutyCycle)

{
    newDutyCycle >= 1.0 && newDutyCycle > 0.0 ? newDutyCycle = 1.0 : newDutyCycle = newDutyCycle;
    
    
    dutyCycle = newDutyCycle * twoPi;
}

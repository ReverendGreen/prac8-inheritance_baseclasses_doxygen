//
//  SquareOscillator.h
//  JuceBasicAudio
//
//  Created by Neil McGuiness on 03/11/2016.
//
//

#ifndef H_SQUAREOSCILLATOR
#define H_SQUAREOSCILLATOR
#include "../JuceLibraryCode/JuceHeader.h"
#include "SinOscillator.h" 

/**
 Class for a sinewave oscillator inherited from Oscillator base class, with the setDutyCycle() function implemented.
 
 @see Oscillator
 
 */

class SquareOscillator : public SinOscillator
{
    
public:
    
    //==============================================================================
    /**K
     
    */
    SquareOscillator();
    
    float renderWaveShape (const float currentPhase) override;
    
    void setDutyCycle(const float newDutyCycle) override;
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phase;
    float phaseInc;
    float twoPi;
    Atomic <float> dutyCycle;

};



#endif /* H_SQUAREOSCILLATOR */
